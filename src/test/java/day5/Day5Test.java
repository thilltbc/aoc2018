package day5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day5Test {

    @Test
    void react() {
        String input = "dabAcCaCBAcCcaDA";
        assertEquals("c", input.substring(4, 5));

        input = Day5.react(input);

        assertEquals("dabCBAcaDA", input);
        assertEquals(10, input.length());
    }

    @Test
    void react2() {
        String input = "ddcdABaddac";
        while (true) {
            String newInput = Day5.react(input);
            if (input == newInput) {
                break;
            } else {
                input = newInput;
            }
        }
        assertEquals("ddcdABaddac", input);
        assertEquals(11, input.length());
    }

    @Test
    void testPrereactions() {
        String input = "dabAcCaCBAcCcaDA";
        assertEquals(4, Day5.runPreReactions(input));
    }
}