package day4;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class Day4RowTest {
    @Test
    public void testFindingCorrectGuardWithMostMinutesSlept() throws IOException {
        List<Day4Row> rows = Day4.loadRows("src/main/resources/input_4_example.txt");
        assert rows.size() > 0;
        assertEquals(10, rows.get(0).getGuard_id());
        assertEquals(Day4.findGuardWithMostMinutesSlept(Day4.getMapOfMapsOfSleepingMinutes(rows)), 10);
    }

    @Test
    public void testFindingMostSleptMinuteForSleepiestGuard() {
        List<Day4Row> rows = Day4.loadRows("src/main/resources/input_4_example.txt");
        Map<Integer, Map<Integer, Integer>> data = Day4.getMapOfMapsOfSleepingMinutes(rows);
        int sleepiest_guard = Day4.findGuardWithMostMinutesSlept(data);
        assertEquals(24, Day4.findMinuteSleptMost(data, sleepiest_guard));
    }
    @Test
    public void testFindGuardAndMinuteSleptMostFrequently() {
        List<Day4Row> rows = Day4.loadRows("src/main/resources/input_4.txt");
        Map<Integer, Map<Integer, Integer>> data = Day4.getMapOfMapsOfSleepingMinutes(rows);
        Map<Integer, Integer> res = Day4.findGuardAndMinuteSleptMostFrequently(data);
        res.entrySet().forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
    }
}