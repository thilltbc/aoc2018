package day6;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day6Test {

    @Test
    void loadInput() throws IOException {
        List<Coord> coords = Day6.loadInput(Files.readAllLines(Paths.get("src/main/resources/input_6_example.txt")));
        assert coords.size() > 0;
        assertEquals("D", coords.get(3).getCharacter());
        System.out.println(coords.toString());
    }

    @Test
    void buildGrid() throws IOException {
        List<Coord> coords = Day6.loadInput(Files.readAllLines(Paths.get("src/main/resources/input_6_example.txt")));
        List<String> grid = Day6.buildGrid(coords);
        Day6.printGrid(grid);
    }

    @Test
    void findClosesetCoord() throws IOException {
        List<Coord> coords = Day6.loadInput(Files.readAllLines(Paths.get("src/main/resources/input_6_example.txt")));
        List<String> grid = Day6.buildGrid(coords);
        String closest1 = Day6.findClosestCoord(coords, 0, 0);
        assertEquals(coords.get(0).getCharacter(), closest1);
    }
}