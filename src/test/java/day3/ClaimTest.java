package day3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClaimTest {
    @Test
    public void canParse() {
        String input = "#123 @ 3,2: 5x4";
        Claim claim = new Claim(input);
        assertEquals(123, claim.getId());
        assertEquals(3, claim.getLeft_offset());
        assertEquals(2, claim.getTop_offset());
        assertEquals(5, claim.getWidth());
        assertEquals(4, claim.getHeight());
    }

}