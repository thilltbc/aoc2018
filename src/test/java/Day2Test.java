import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day2Test {

    @Test
    void findTwoClosestIDs() {
        List<String> items = List.of("abcde","fghij", "klmno","pqrst","fguij","axcye","wvxyz");
        List<String> closeIDs = Day2.findTwoClosestIDs(items);
        String match = Day2.getMatchingSection(closeIDs.get(0), closeIDs.get(1));
        assertEquals("fgij", match, "Should find common match");

    }
    @Test
    void findMaxLetterRepeats() {
        List<String> items = List.of("abcdef","bababc", "abbcde","abcccd","aabcdd","abcdee","ababab");
        int twos = 0;
        int threes = 0;
        for (String item: items) {
            List<Integer> repeats = Day2.findLetterRepeats(item);
            if(repeats.contains(2)) {
                twos++;
            }
            if(repeats.contains(3)) {
                threes++;
            }
        }
        assertEquals(3, threes, "Expected 3 3's");
        assertEquals(4, twos, "Expected 4 2's");


    }
}