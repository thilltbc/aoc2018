package day3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day3 {
    public static void main(String[] args) throws IOException {
        List<String> input = Files.readAllLines(Paths.get("src/main/resources/input_3.txt"));
        int[][] fabric = createFabricGrid(10000,10000);
        for (String in : input) {
            fabric = rasterizeFabric(fabric, new Claim(in));
        }
        System.out.println(countOverlaps(fabric));
        for (String in : input) {
            if(!doesClaimOverlap(fabric, new Claim(in))) {
                System.out.println(new Claim(in).getId() + " Doesnt overlap!");
            }
        }

    }
    public static int[][] createFabricGrid(int width, int height) {
        // should initialize to 0
        int[][] fabric = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                fabric[i][j] = 0;
            }
        }
        return fabric;
    }
    public static void printGrid(int[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.print('\n');
        }
    }
    public static int[][] rasterizeFabric(int[][] fabric, Claim claim) {
        int height = claim.getHeight();
        int width = claim.getWidth();
        int left_offset = claim.getLeft_offset();
        int top_offset = claim.getTop_offset();

        for (int i = top_offset; i < top_offset + height; i++) {
            for (int j = left_offset; j < left_offset + width; j++) {
                fabric[i][j]++;
            }
        }
        return fabric;
    }
    public static int countOverlaps(int[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if(grid[i][j] > 1)
                    count++;
            }
        }
        return count;
    }
    public static boolean doesClaimOverlap(int[][] fabric, Claim claim) {
        int height = claim.getHeight();
        int width = claim.getWidth();
        int left_offset = claim.getLeft_offset();
        int top_offset = claim.getTop_offset();
        boolean overlaps = false;

        for (int i = top_offset; i < top_offset + height; i++) {
            for (int j = left_offset; j < left_offset + width; j++) {
                if(fabric[i][j] > 1)
                    overlaps = true;
            }
        }
        return overlaps;
    }

}
