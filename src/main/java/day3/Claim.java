package day3;

import lombok.Data;

@Data
public class Claim {
    private int id;
    private int left_offset;
    private int top_offset;
    private int height;
    private int width;

    public Claim(String text) {
        String[] splitStr = text.split(" ");
        this.id = Integer.parseInt(splitStr[0].replaceAll("#",""));
        String[] offsets = splitStr[2].split(",");
        this.left_offset = Integer.parseInt(offsets[0]);
        this.top_offset = Integer.parseInt(offsets[1].replaceAll(":", ""));
        String[] dimensions = splitStr[3].split("x");
        this.width = Integer.parseInt(dimensions[0]);
        this.height = Integer.parseInt(dimensions[1]);
    }
}
