import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Day2 {
    public static void main(String[] args) throws IOException {
        List<String> input = Files.readAllLines(Paths.get("src/main/resources/input_2.txt"));
        List<String> closeIDs = Day2.findTwoClosestIDs(input);
        String match = Day2.getMatchingSection(closeIDs.get(0), closeIDs.get(1));
        System.out.println(match);
    }
    public static Integer findChecksum(List<String> items) {
        int twos = 0;
        int threes = 0;
        for (String item: items) {
            List<Integer> repeats = Day2.findLetterRepeats(item);
            if(repeats.contains(2)) {
                twos++;
            }
            if(repeats.contains(3)) {
                threes++;
            }
        }
        return twos*threes;
    }
    public static List<Integer> findLetterRepeats(String in) {
        List<Integer> repeats = new LinkedList<>();
        Map<String, Integer> matches = new HashMap<>();
        for (char c : in.toCharArray()) {
            String s = String.valueOf(c);
            if (matches.containsKey(s)) {
                matches.put(s, matches.get(s) + 1);
            } else {
                matches.put(s, 1);
            }
        }
        for (Map.Entry<String, Integer> e : matches.entrySet()) {
            repeats.add(e.getValue());
        }
        return repeats;
    }

    public static List<String> findTwoClosestIDs(List<String> in) {
        //iterate id
        //compare against all other ids in list
        //find two ids who share 4/5 characters as matches, including position
        List<String> matches = new LinkedList<>();
        for(String id: in)  {
            String match =  in.stream()
                    .filter(i -> almostMatchesString(id, i))
                    .findFirst()
                    .orElseGet(String::new);
            if(!match.isEmpty()) {
                matches.add(id);
                matches.add(match);
                return matches;
            }
        }
        return matches;
    }
    public static boolean almostMatchesString(String a, String b) {
        // if x-1/x characters are same value/position, return true
        if(a.length() != b.length()) {
            return false;
        }
        int matchCount = 0;
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) == b.charAt(i))
                matchCount++;
        }
        return matchCount == a.length()-1;
    }
    public static String getMatchingSection(String a, String b) {
        // if x-1/x characters are same value/position, return true
        if(a.length() != b.length()) {
            return "";
        }
        String match = "";
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) == b.charAt(i))
                match += a.charAt(i);
        }
        return match;
    }

}
