import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class Day1 {
    public static void main(String[] args) throws IOException {
        List<String> input = Files.readAllLines(Paths.get("src/main/resources/input_1.txt"));

        System.out.println(findFirstRepeat(input));

    }
    public static Integer findFirstRepeat(List<String> input) {
        Integer frequency = 0;
        List<Integer> freq_history = new LinkedList<>();
        freq_history.add(frequency);
        for (int i = 0; i < input.size(); ) {
            String in = input.get(i);
            Integer freq_change =  Integer.parseInt(in.substring(1));

            switch(in.substring(0,1)) {
                case "+":
                    frequency += freq_change;
                    break;
                case "-":
                    frequency -= freq_change;
                    break;
                default:
                    System.out.println("Unknown symbol!");
                    break;
            }
            if(freq_history.contains(frequency)) {
                freq_history.add(frequency);
                return frequency;
            }
            freq_history.add(frequency);
            if(i==input.size()-1) {
                i = 0; //reset loop
                System.out.println("Resetting loop");
            } else {
                ++i;
            }
        }
        return null; // didn't find it
    }
}
